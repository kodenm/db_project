CREATE OR REPLACE FUNCTION create_stock_records()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS $$
DECLARE
    operation INT;
    datetime_now TIMESTAMPTZ;
BEGIN

    SELECT id INTO operation FROM public.operation op WHERE op.name = 'Create' LIMIT 1;
    SELECT NOW() INTO datetime_now;

    INSERT INTO log.stock_change_record (
        stock_id, 
        operation_id, 
        amount, 
        date) 
        VALUES (NEW.id, operation, NEW.amount, datetime_now);

    INSERT INTO log.stock_price_change_record (
        stock_id, 
        changed_at, 
        old_purchase_price,
        old_registry_price,
        old_retail_price,
        old_wholesale_price,
        new_purchase_price,
        new_registry_price,
        new_retail_price,
        new_wholesale_price)
        VALUES (NEW.id, datetime_now, 0, 0, 0, 0, NEW.purchase_price, NEW.registry_price, NEW.retail_price, NEW.wholesale_price);

    RETURN NEW;
END;
$$

CREATE OR REPLACE FUNCTION update_stock_records()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS $$
DECLARE
    operation INT;
    datetime_now TIMESTAMPTZ;
BEGIN

    SELECT NOW() INTO datetime_now;

    IF NEW.amount > OLD.amount THEN
        SELECT id INTO operation FROM public.operation op WHERE op.name = 'Increase' LIMIT 1;

        INSERT INTO log.stock_change_record (
            stock_id, 
            operation_id, 
            amount, 
            date) 
            VALUES (NEW.id, operation, NEW.amount - OLD.amount, datetime_now);
    ELSIF NEW.amount < OLD.amount THEN
        SELECT id INTO operation FROM public.operation op WHERE op.name = 'Decrease' LIMIT 1;

        INSERT INTO log.stock_change_record (
            stock_id, 
            operation_id, 
            amount, 
            date) 
            VALUES (NEW.id, operation, OLD.amount - NEW.amount, datetime_now);
    END IF;
    

    IF 
        NEW.purchase_price <> OLD.purchase_price OR
        NEW.registry_price <> OLD.registry_price OR
        NEW.retail_price <> OLD.retail_price OR
        NEW.wholesale_price <> OLD.wholesale_price
    THEN
        INSERT INTO log.stock_price_change_record (
            stock_id, 
            changed_at, 
            old_purchase_price,
            old_registry_price,
            old_retail_price,
            old_wholesale_price,
            new_purchase_price,
            new_registry_price,
            new_retail_price,
            new_wholesale_price)
        VALUES (
            NEW.id, 
            datetime_now, 
            OLD.purchase_price, 
            OLD.registry_price, 
            OLD.retail_price, 
            OLD.wholesale_price, 
            NEW.purchase_price, 
            NEW.registry_price, 
            NEW.retail_price, 
            NEW.wholesale_price);
    END IF;
    RETURN NEW;
END;
$$

CREATE TRIGGER create_stock 
    AFTER INSERT ON public.stock
    FOR EACH ROW
    EXECUTE PROCEDURE create_stock_records();

CREATE TRIGGER update_stock
    AFTER UPDATE ON public.stock
    FOR EACH ROW
    EXECUTE PROCEDURE update_stock_records();
