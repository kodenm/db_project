ALTER TABLE supplement
ADD CONSTRAINT fk_good_supplement FOREIGN KEY (good_id) REFERENCES good (id) ON DELETE NO ACTION;

ALTER TABLE stock
ADD CONSTRAINT fk_warehouse_stock FOREIGN KEY (warehouse_id) REFERENCES warehouse (id) ON DELETE NO ACTION;
ALTER TABLE stock
ADD CONSTRAINT fk_supplement_stock FOREIGN KEY (supplement_id) REFERENCES supplement (id) ON DELETE NO ACTION;

ALTER TABLE operation
ADD CONSTRAINT uq_operation_name UNIQUE (name);

ALTER TABLE log.stock_change_record
ADD CONSTRAINT fk_stock_stock_change_record FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE NO ACTION;
ALTER TABLE log.stock_change_record
ADD CONSTRAINT fk_operation_stock_change_record FOREIGN KEY (operation_id) REFERENCES operation (id) ON DELETE NO ACTION;

ALTER TABLE log.stock_price_change_record
ADD CONSTRAINT fk_stock_stock_price_change_record FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE NO ACTION;