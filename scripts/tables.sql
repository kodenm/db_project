CREATE TABLE good (
    id BIGSERIAL,
    name VARCHAR(128) NOT NULL,
    units VARCHAR(16),
    shelf_life INT,
    CONSTRAINT pk_good PRIMARY KEY (id) 
);

CREATE TABLE supplement (
    id BIGSERIAL,
    good_id BIGINT,
    name VARCHAR(128) NOT NULL,
    produced_at TIMESTAMPTZ NOT NULL,
    CONSTRAINT pk_supplement PRIMARY KEY (id)
);

CREATE TABLE stock (
    id BIGSERIAL,
    warehouse_id BIGINT,
    supplement_id BIGINT,
    amount REAL NOT NULL,
    purchase_price REAL,
    registry_price REAL,
    retail_price REAL,
    wholesale_price REAL,
    CONSTRAINT pk_stock PRIMARY KEY (id)
);

CREATE TABLE warehouse (
    id BIGSERIAL,
    name VARCHAR(128) NOT NULL,
    address VARCHAR(128) NOT NULL,
    CONSTRAINT pk_warehouse PRIMARY KEY (id)
);

CREATE TABLE operation (
    id BIGSERIAL,
    name VARCHAR(64) NOT NULL,
    CONSTRAINT pk_operation PRIMARY KEY (id)
);

CREATE SCHEMA log;

CREATE TABLE log.stock_change_record (
    id BIGSERIAL,
    stock_id BIGINT,
    operation_id BIGINT,
    amount REAL NOT NULL,
    date TIMESTAMPTZ NOT NULL,
    CONSTRAINT pk_stock_change_record PRIMARY KEY (id)
);

CREATE TABLE log.stock_price_change_record (
    id BIGSERIAL,
    stock_id INT,
    changed_at TIMESTAMPTZ NOT NULL,
    old_purchase_price REAL,
    old_registry_price REAL,
    old_retail_price REAL,
    old_wholesale_price REAL,
    new_purchase_price REAL,
    new_registry_price REAL,
    new_retail_price REAL,
    new_wholesale_price REAL,
    CONSTRAINT pk_stock_price_change_record PRIMARY KEY (id)
);