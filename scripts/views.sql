CREATE OR REPLACE VIEW stock_good_details AS
    SELECT 
        st.id AS "Stock ID",
        sp.id AS "Supplement ID",
        sp.name AS "Supplement Name",
        g.name AS "Good Name",
		g.shelf_life AS "Shelf Life",
		st.amount AS "Left",
        sp.produced_at AS "Produced At",
        sp.produced_at + make_interval(days => g.shelf_life) AS "Valid until",
        wh.name AS "Warehouse Name",
        wh.address AS "Warehouse Address"
    FROM public.stock AS st
    INNER JOIN public.warehouse wh ON wh.id = st.warehouse_id
    INNER JOIN public.supplement sp ON st.supplement_id = sp.id
    INNER JOIN public.good g ON g.id = sp.good_id;

CREATE OR REPLACE VIEW stock_warehouse_details AS
    SELECT
        st.id AS "Stock ID",
        amount AS "Left",
        wh.id AS "Warehouse ID",
        wh.name AS "Warehouse Name",
        wh.address AS "Warehouse Address"
    FROM public.stock AS st
    INNER JOIN public.warehouse wh ON wh.id = st.warehouse_id

CREATE OR REPLACE VIEW stock_change_log AS
    SELECT
        lg.id,
        op.name AS "Operation",
        lg.amount AS "Difference",
        lg.date AS "Date"
    FROM log.stock_change_record as lg
    INNER JOIN public.operation op ON op.id = lg.operation_id; 